/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.Objects;

/**
 *
 * @author PauloGouveia
 */
public class OSModel {
    
    private int idServico;
   private String nomeCliente;
  private  String cpf;
  private  String endereco;
  private  String bairro;
  private  String defeito;
  private  String dataChegada;
   private String dataEntrega;
   private Double valorEntrada;
  private  String observacao;

    public int getIdServico() {
        return idServico;
    }

    public void setIdServico(int idServico) {
        this.idServico = idServico;
    }

    public String getNomeCliente() {
        return nomeCliente;
    }

    public void setNomeCliente(String nomeCliente) {
        this.nomeCliente = nomeCliente;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public String getDefeito() {
        return defeito;
    }

    public void setDefeito(String defeito) {
        this.defeito = defeito;
    }

    public String getDataChegada() {
        return dataChegada;
    }

    public void setDataChegada(String dataChegada) {
        this.dataChegada = dataChegada;
    }

    public String getDataEntrega() {
        return dataEntrega;
    }

    public void setDataEntrega(String dataEntrega) {
        this.dataEntrega = dataEntrega;
    }

    public Double getValorEntrada() {
        return valorEntrada;
    }

    public void setValorEntrada(Double valorEntrada) {
        this.valorEntrada = valorEntrada;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

  
  
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 83 * hash + this.idServico;
        hash = 83 * hash + Objects.hashCode(this.nomeCliente);
        hash = 83 * hash + Objects.hashCode(this.cpf);
        hash = 83 * hash + Objects.hashCode(this.endereco);
        hash = 83 * hash + Objects.hashCode(this.bairro);
        hash = 83 * hash + Objects.hashCode(this.defeito);
        hash = 83 * hash + Objects.hashCode(this.dataChegada);
        hash = 83 * hash + Objects.hashCode(this.dataEntrega);
        hash = 83 * hash + Objects.hashCode(this.valorEntrada);
        hash = 83 * hash + Objects.hashCode(this.observacao);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final OSModel other = (OSModel) obj;
        if (this.idServico != other.idServico) {
            return false;
        }
        if (!Objects.equals(this.nomeCliente, other.nomeCliente)) {
            return false;
        }
        if (!Objects.equals(this.cpf, other.cpf)) {
            return false;
        }
        if (!Objects.equals(this.endereco, other.endereco)) {
            return false;
        }
        if (!Objects.equals(this.bairro, other.bairro)) {
            return false;
        }
        if (!Objects.equals(this.defeito, other.defeito)) {
            return false;
        }
        if (!Objects.equals(this.dataChegada, other.dataChegada)) {
            return false;
        }
        if (!Objects.equals(this.dataEntrega, other.dataEntrega)) {
            return false;
        }
        if (!Objects.equals(this.observacao, other.observacao)) {
            return false;
        }
        if (!Objects.equals(this.valorEntrada, other.valorEntrada)) {
            return false;
        }
        return true;
    }

  
  
    @Override
    public String toString() {
        return "OSModel{" + "idServico=" + idServico + ", nomeCliente=" + nomeCliente + ", cpf=" + cpf + ", endereco=" + endereco + ", bairro=" + bairro + ", defeito=" + defeito + ", dataChegada=" + dataChegada + ", dataEntrega=" + dataEntrega + ", valorEntrada=" + valorEntrada + ", observacao=" + observacao + '}';
    }

    
}
