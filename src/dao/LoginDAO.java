/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import Model.LoginModel;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author PauloGouveia
 */
public class LoginDAO {

    Conexao conexao = new Conexao();
    Connection conex = conexao.criarConexao();

    public int salvarLogin(LoginModel login) {
        String sqlInsert = " INSERT "
                + " INTO"
                + " login (usuario, senha)"
                + " VALUES(?,?)";

        try {
            PreparedStatement preparacaoSalvar = conex.prepareStatement(sqlInsert);

            preparacaoSalvar.setString(1, login.getUsuario());
            preparacaoSalvar.setString(2, login.getSenha());

            preparacaoSalvar.executeUpdate();
        } catch (Exception ex) {
            ex.printStackTrace();
            Logger.getLogger(LoginDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return -1;
    }

    public List<LoginModel> listarTodosLogin() {
        String sqlSelectAll = "SELECT * FROM login";
        List<LoginModel> listaDeTodosLogin = new ArrayList<LoginModel>();
        try {

            PreparedStatement preparacaoSelectAll = conex.prepareStatement(sqlSelectAll);
            ResultSet resultadoListaTodos = preparacaoSelectAll.executeQuery();

            while (resultadoListaTodos.next()) {
                LoginModel login = new LoginModel();
                login.setCodigoLogin(resultadoListaTodos.getInt("codigo_login"));
                login.setUsuario(resultadoListaTodos.getString("usuario"));
                login.setSenha(resultadoListaTodos.getString("senha"));

                listaDeTodosLogin.add(login);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
            Logger.getLogger(LoginModel.class.getName()).log(Level.SEVERE, null, ex);

            return null;

        }
        return listaDeTodosLogin;
    }

    public void ExcluirLogin(LoginModel login) {
        String sqlDelete = "delete from login"
                + " where codigo_login = ?";

        try {
            PreparedStatement preparacaoDelete = conex.prepareStatement(sqlDelete);
            preparacaoDelete.setInt(1, login.getCodigoLogin());
            preparacaoDelete.execute();
        } catch (Exception ex) {
            ex.printStackTrace();
            Logger.getLogger(LoginModel.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public boolean validarLogin(String usuario, String senha) {

        boolean check = false;

        String sqlSelectCheckLogin = "SELECT * FROM login "
                + " WHERE usuario = ? AND senha = ? ";

        try {

            PreparedStatement preparacaoSqlSelectCheckLogin = conex.prepareStatement(sqlSelectCheckLogin);
            preparacaoSqlSelectCheckLogin.setString(1, usuario);
            preparacaoSqlSelectCheckLogin.setString(2, senha);
            ResultSet resultadoCheckLogin = preparacaoSqlSelectCheckLogin.executeQuery();

            if (resultadoCheckLogin.next()) {

                check = true;
            }

        } catch (Exception ex) {
            ex.printStackTrace();
            Logger.getLogger(LoginModel.class.getName()).log(Level.SEVERE, null, ex);

        }
        return check;
    }
}
