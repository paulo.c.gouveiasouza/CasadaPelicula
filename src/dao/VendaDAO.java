/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import Model.VendasModel;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author PauloGouveia
 */
public class VendaDAO {

    Conexao conexao = new Conexao();
    Connection conex = conexao.criarConexao();

    public int salvarVenda(VendasModel venda) {
        String sqlInsert = " INSERT "
                + " INTO"
                + " venda ( nome_venda, valor_unitario, quantidade, data_venda, valor_total, descricao)"
                + " VALUES(?,?,?,?,?,?)";

        try {
            PreparedStatement preparacaoSalvar = conex.prepareStatement(sqlInsert);

            preparacaoSalvar.setString(1, venda.getNomeVenda());
            preparacaoSalvar.setDouble(2, venda.getValorUnitario());
            preparacaoSalvar.setDouble(3, venda.getQuantidade());
            preparacaoSalvar.setString(4, venda.getDataVenda());
            preparacaoSalvar.setDouble(5, venda.getValorTotal());
            preparacaoSalvar.setString(6, venda.getDescricao());

            preparacaoSalvar.executeUpdate();
        } catch (Exception ex) {
            ex.printStackTrace();
            Logger.getLogger(VendaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return -1;
    }

    public List<VendasModel> listarTodasVendas() {
        String sqlSelectAll = "SELECT * FROM venda";
        List<VendasModel> listaDeTodasVenda = new ArrayList<VendasModel>();
        try {

            PreparedStatement preparacaoSelectAll = conex.prepareStatement(sqlSelectAll);
            ResultSet resultadoListaTodos = preparacaoSelectAll.executeQuery();

            while (resultadoListaTodos.next()) {
                VendasModel venda = new VendasModel();
                venda.setCodigoVenda(resultadoListaTodos.getInt("codigo_venda"));
                venda.setNomeVenda(resultadoListaTodos.getString("nome_venda"));
                venda.setValorUnitario(resultadoListaTodos.getDouble("valor_unitario"));
                venda.setQuantidade(resultadoListaTodos.getDouble("quantidade"));
                venda.setDataVenda(resultadoListaTodos.getString("data_venda"));
                venda.setValorTotal(resultadoListaTodos.getDouble("valor_total"));
                venda.setDescricao(resultadoListaTodos.getString("descricao"));

                listaDeTodasVenda.add(venda);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
            Logger.getLogger(VendasModel.class.getName()).log(Level.SEVERE, null, ex);

            return null;

        }
        return listaDeTodasVenda;
    }

    public void ExcluirVenda(VendasModel venda) {
        String sqlDelete = "delete from venda"
                + " where codigo_venda = ?";

        try {
            PreparedStatement preparacaoDelete = conex.prepareStatement(sqlDelete);
            preparacaoDelete.setInt(1, venda.getCodigoVenda());
            preparacaoDelete.execute();
        } catch (Exception ex) {
            ex.printStackTrace();
            Logger.getLogger(VendasModel.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
