/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import model.OSModel;


/**
 *
 * @author PauloGouveia
 */
public class OrdemServicoDAO {

    Conexao conexao = new Conexao();
    Connection conex = conexao.criarConexao();

    public int salvarOrdemServico(OSModel ordemServico) {
        String sqlInsert = " INSERT "
                + " INTO"
                + " ordem_servico (nome_cliente, cpf, endereco, bairro, defeito, data_chegada, data_prev_entrega, valor_entrada, observacao)"
                + " VALUES(?,?,?,?,?,?,?,?,?)";

        try {
            PreparedStatement preparacaoSalvar = conex.prepareStatement(sqlInsert);

            preparacaoSalvar.setString(1, ordemServico.getNomeCliente());
            preparacaoSalvar.setString(2, ordemServico.getCpf());
            preparacaoSalvar.setString(3, ordemServico.getEndereco());
            preparacaoSalvar.setString(4, ordemServico.getBairro());
            preparacaoSalvar.setString(5, ordemServico.getDefeito());
            preparacaoSalvar.setString(6, ordemServico.getDataChegada());
            preparacaoSalvar.setString(7, ordemServico.getDataEntrega());
            preparacaoSalvar.setDouble(8, ordemServico.getValorEntrada());
            preparacaoSalvar.setString(9, ordemServico.getObservacao());

            preparacaoSalvar.executeUpdate();
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex);
            Logger.getLogger(OrdemServicoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return -1;
    }

    public List<OSModel> listarTodosServiços() {
        String sqlSelectAll = "SELECT * FROM ordem_servico";
        List<OSModel> listaDeTodasServico = new ArrayList<OSModel>();
        try {

            PreparedStatement preparacaoSelectAll = conex.prepareStatement(sqlSelectAll);
            ResultSet resultadoListaTodos = preparacaoSelectAll.executeQuery();

            while (resultadoListaTodos.next()) {
                OSModel servico = new OSModel();
                servico.setIdServico(resultadoListaTodos.getInt("id_serviço"));
                servico.setNomeCliente(resultadoListaTodos.getString("nome_cliente"));
                servico.setCpf(resultadoListaTodos.getString("cpf"));
                servico.setEndereco(resultadoListaTodos.getString("endereco"));
                servico.setBairro(resultadoListaTodos.getString("bairro"));
                servico.setDefeito(resultadoListaTodos.getString("defeito"));
                servico.setDataChegada(resultadoListaTodos.getString("data_chegada"));
                servico.setDataEntrega(resultadoListaTodos.getString("data_prev_entrega"));
                servico.setValorEntrada(resultadoListaTodos.getDouble("valor_entrada"));
                servico.setObservacao(resultadoListaTodos.getString("observacao"));

                listaDeTodasServico.add(servico);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
            Logger.getLogger(OSModel.class.getName()).log(Level.SEVERE, null, ex);

            return null;

        }
        return listaDeTodasServico;
    }

    public void ExcluirServico(OSModel servico) {
        String sqlDelete = "delete from ordem_servico"
                + " where id_serviço = ?";

        try {
            PreparedStatement preparacaoDelete = conex.prepareStatement(sqlDelete);
            preparacaoDelete.setInt(1, servico.getIdServico());
            preparacaoDelete.execute();
        } catch (Exception ex) {
            ex.printStackTrace();
            Logger.getLogger(OSModel.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
